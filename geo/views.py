# Create your views here.
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response, render, redirect,get_object_or_404
from django.template import RequestContext
from geo.forms import RegistrationForm, LoginForm
from geo.models import *
from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate, login, logout
from django.template.loader import render_to_string
from django.db import connection
from django.core.serializers.json import DjangoJSONEncoder
from django.http import Http404, HttpResponseBadRequest
from django.conf import settings
import json
import urllib2
from collections import OrderedDict
from django.core import serializers
from time import strftime
from django.db import connection
from collections import defaultdict
from reportlab.pdfgen import canvas
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import *
from reportlab.lib import colors

def tree():
    return defaultdict(tree)


def index(request):
        if request.user.is_authenticated():
                zoom_point=str(request.session.get('zoom_point'))
                zoom=str(request.session.get('zoom'))
                id_provider=str(request.session.get('id_provider'))
                id_societe=str(request.session.get('id_societe'))
                id_connexion=str(request.session.get('id_connexion'))
                
                cursor = connection.cursor()
                cursor.execute("select mot_fr as francais,mot_en as anglais,mot_ar as arabe, index_mot as index_mot from traduction")
                items = cursor.fetchall()
                
                for row in items:
                    if row[3] == "tempsreel":
                        tempsreel=row[1]
                        
                    elif row[3] == "vitessekmh":
                        vitessekmh=row[1]
                        
                    elif  row[3] == "temperature":
                        temperature = row[1]
                        
                    elif  row[3] == "carburantvehicule":
                        carburantvehicule = row[1]
                    elif  row[3] == "etat":
                        etat = row[1]    
                
                tempsreel="Real Time"
                vitessekmh="Km/h"
                temperature="T "
                carburantvehicule="Fuel"
                etat="reports"
                idview ='v_'+id_provider+'_'+id_societe+'_'+id_connexion
                return render(request, "login.html",{
                    'tempsreel': tempsreel,
                    'vitessekmh':vitessekmh,
                    'temperature':temperature,
                    'carburantvehicule':carburantvehicule,
                    'etat':etat,
                    'idview':idview,
                    'id_societe':id_societe,
                    'zoom_point':zoom_point[1:-1],
                    'zoom':zoom,
                
                    })
                ''' user is not submitting the form, show the login form '''
        else:     
                return HttpResponseRedirect('/login/')
              
def json_get_latest_waypoint(request):
    if request.user.is_authenticated():

                zoom_point=str(request.session.get('zoom_point'))
                zoom=str(request.session.get('zoom'))
                id_provider=str(request.session.get('id_provider'))
                id_societe=str(request.session.get('id_societe'))
                id_connexion=str(request.session.get('id_connexion'))

                query_get_data_from_specific_view = 'SELECT * FROM v_'+id_provider+'_'+id_societe+'_'+id_connexion+' a1,'
                query_get_data_from_specific_view +=' (SELECT voiture, max(dateheure) AS last_date FROM v_'+id_provider+'_'+id_societe+'_'+id_connexion+' GROUP BY v_'+id_provider+'_'+id_societe+'_'+id_connexion+'.voiture) a2 '
                query_get_data_from_specific_view +='WHERE a1.voiture = a2.voiture '
                query_get_data_from_specific_view +='AND a1.dateheure = a2.last_date '
                to_json = []
                
                
                cursor = connection.cursor()
                
                
                items = Waypoint.objects.raw(query_get_data_from_specific_view)
                cursor = connection.cursor()
                cursor.execute("BEGIN")
                
                for waypoint in items:
                    waypoint_dict=tree()
#                     waypoint_dict = OrderedDict()
                    waypoint_dict['id'] = str(waypoint.id)
                    waypoint_dict['position'] = str(waypoint.positio)
                    waypoint_dict['dop'] = waypoint.dop
                    waypoint_dict['altitude'] = waypoint.alltitude
                    waypoint_dict['etat'] = waypoint.etat
                    waypoint_dict['dateheure'] = waypoint.dateheure.strftime("%d-%m %H:%M")
                    waypoint_dict['vitesse'] = waypoint.vitesse
                    waypoint_dict['voiture'] = waypoint.voiture
                    waypoint_dict['cap'] = waypoint.cap
                    waypoint_dict['disponible'] = waypoint.disponible               
                    waypoint_dict['geo_pos_X'] = str(waypoint.geo_pos.x)
                    waypoint_dict['geo_pos_Y'] = str(waypoint.geo_pos.y)
                    
                    waypoint_dict['inputs'] = waypoint.inputs
                    waypoint_dict['outputs'] = waypoint.outputs
                    if waypoint.angle is not None:
                        waypoint_dict['angle'] = waypoint.angle
                    else:
                        waypoint_dict['angle'] = 0
                    waypoint_dict['temperature'] = waypoint.temperature
                    waypoint_dict['etat_porte'] = waypoint.etat_porte
                    waypoint_dict['niveau_carburant'] = waypoint.niveau_carburant
#                     waypoint_dict['id_delegation'] = waypoint.id_delegation
#                     waypoint_dict['id_ville'] = waypoint.id_ville
#                     waypoint_dict['id_localite'] = waypoint.id_localite
                    waypoint_dict['batterie'] = waypoint.batterie
                    waypoint_dict['kilometarge'] = waypoint.kilometarge
                    waypoint_dict['alimentation'] = waypoint.alimentation
                    waypoint_dict['gps_valid'] = waypoint.gps_valid
                    waypoint_dict['id_voiture'] = waypoint.id_voiture
                    if waypoint.flag_geocode == 1:
                        waypoint_dict['adresse']=waypoint.adresse
                    #else:
                        #url = "http://nominatim.openstreetmap.org/reverse?format=json&lat="+str(waypoint.geo_pos.y)+"&lon="+str(waypoint.geo_pos.x)+"&zoom=18&addressdetails=1"
                        #data = json.load(urllib2.urlopen(url))
                        #adresse =''
                         
                        #if 'road' in data['address'].keys():
                            #adresse += data['address']['road'].encode('UTF-8')
                         
                        #if 'city' in data['address'].keys():
                            #adresse += ', '+ data['address']['city'].encode('UTF-8')
                        #if 'state' in data['address'].keys():
                            #adresse += ', '+ data['address']['state'].encode('UTF-8')
##                         adresse = Geocoder.reverse_geocode(waypoint.geo_pos.y, waypoint.geo_pos.x)                                               
                        #waypoint_dict['adresse']=adresse                      
                        #query_update_adresse="UPDATE archive SET  adresse='%s ', flag_geocode = 1 where idarchive = %s " % (adresse.replace("'"," "),waypoint.id)
                        #cursor.execute(query_update_adresse)
                        #cursor.execute("COMMIT")
                        
                    #query_get_fuel_level = 'select idarchive as id, niveau_carburant as value, dateheure as date from archive where voiture =%s AND niveau_carburant >0 order by dateheure limit 5'  %(waypoint.id_voiture)
                    #f_consom = Fuel.objects.raw(query_get_fuel_level)
                    #to_json_f_consom = []
                    #for fuel in f_consom:
                        #fuel_dict = OrderedDict()
                        #fuv1_dict = OrderedDict()
                        #fuv2_dict = OrderedDict()
                        #to_fu = []      
                        #fuv1_dict['v']="Date("+str(fuel.date)[:4]+","+ str(fuel.date)[5:7]+","+ str(fuel.date)[8:10]+","+str(fuel.date)[11:13]+","+str(fuel.date)[14:16]+","+str(fuel.date)[17:19]+")"              
                        #to_fu.append(fuv1_dict)                    
                        #fuv2_dict['v']=fuel.value
                        #to_fu.append(fuv2_dict)                    
                        #fuel_dict['c'] = to_fu
                        #to_json_f_consom.append(fuel_dict)
                        
                        
                        
#                         fuel_dict = OrderedDict()
#                         fuv1_dict = OrderedDict()
#                         fuv2_dict = OrderedDict()
#                         to_fu = []      
#                         fuv1_dict['v']="Date("+str(fuel.date)[:4]+","+ str(fuel.date)[5:7]+","+ str(fuel.date)[8:10]+","+str(fuel.date)[11:13]+","+str(fuel.date)[14:16]+","+str(fuel.date)[17:19]+")"              
#     #                     fuv1_dict['v']=str(fuel.date)
#                         to_fu.append(fuv1_dict)                    
#                         fuv2_dict['v']=fuel.value
#                         to_fu.append(fuv2_dict)                    
#                         fuel_dict['c'] = to_fu
#                         to_json_f_consom.append(fuel_dict)
#                                             
#                     cols = 'cols'
#                     schema = '[{label: Date, type: datetime},'
#                     schema+='{label: carburant, type: number}]'
#                     root_name = 'rows' # or it can be queryset.model._meta.verbose_name_plural 
#                     data = '{%s: %s,%s: %s }' % (cols,schema, root_name, to_json_f_consom) 
                    
#                     waypoint_dict['Fuel_Consom'] = data
            
                    #f_cols=[]
                    #fuv1_dict = OrderedDict()
                    #fuv2_dict = OrderedDict()
                    #fuv1_dict['label']="Date"
                    #fuv1_dict['type']="datetime"
                    #f_cols.append(fuv1_dict)
                    
                    #fuv2_dict['label']="carburant"
                    #fuv2_dict['type']="number"
                    #f_cols.append(fuv2_dict)
                    #fuel_consom_array = OrderedDict()
                    #fuel_consom_array['cols']=f_cols
                    #fuel_consom_array['rows']=to_json_f_consom
                    #waypoint_dict['Fuel_Consom']=fuel_consom_array
                    
#                     query_get_ride_level = 'select DISTINCT (geo_pos) as geo_position, idarchive as id, X(geo_pos) as lng, Y(geo_pos) as lat, dateheure as date,vitesse as vitesse,temperature as temperature,niveau_carburant as carburant, adresse as location from archive where voiture =%s order by dateheure ASC limit 30' %(waypoint.id_voiture)
#                     rides = Ride.objects.raw(query_get_ride_level)
#                     to_json_ride =[]
#                     for pos in rides:
#                         ride_dict = OrderedDict()
#                         ride_dict['geo_pos_X'] = pos.lng
#                         ride_dict['geo_pos_Y'] = pos.lat
#                         ride_dict['vitesse'] = pos.vitesse
#                         ride_dict['temperature'] = pos.temperature
#                         ride_dict['carburant'] = pos.carburant
#                         ride_dict['date']=pos.date.strftime("%m-%d %H:%M")
#                         ride_dict['location']=pos.location
#     
#                         
#                         to_json_ride.append(ride_dict)
#  
#                     waypoint_dict['Fuel_Consom']['cols']=f_cols
#                     waypoint_dict['Fuel_Consom']['rows']=to_json_f_consom 
#                     waypoint_dict['Ride']=to_json_ride
                    
                    to_json.append(waypoint_dict)
                
                root_name = 'rows' # or it can be queryset.model._meta.verbose_name_plural 
                data = '{"page": "1",\n "total": %s,\n "records": "20",\n "%s": %s}' % (to_json.__len__(), root_name, json.dumps(to_json, cls=DjangoJSONEncoder,indent=4)) 
                
                #js = '{root:%s,count:%s}' % (serializers.serialize('json', to_json, use_natural_keys=True), to_json.count())
                return HttpResponse(data, content_type='application/json')
#                 items=json.dumps(to_json, cls=DjangoJSONEncoder,indent=4)
#                 return HttpResponse(items, content_type='application/json')
#

def get_report_from_object(request):
    if request.user.is_authenticated():
        s = request.GET.get('data', '').encode('UTF-8')
    # Create the HttpResponse object with the appropriate PDF headers.
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="report_'+s+'.pdf"'
        query_get_ride_level = 'select DISTINCT (geo_pos) as geo_position, idarchive as id, X(geo_pos) as lng, Y(geo_pos) as lat, dateheure as date,vitesse as vitesse,temperature as temperature,niveau_carburant as carburant, adresse as adresse from archive where voiture =%s order by dateheure DESC limit 50' %(s)
        items = Ride.objects.raw(query_get_ride_level)
        # Our container for 'Flowable' objects
        elements = []
         
        # A large collection of style sheets pre-made for us
        styles = getSampleStyleSheet()
         
        # A basic document for us to write to 'rl_hello_table.pdf'
        doc = SimpleDocTemplate(response)
         
        elements.append(Paragraph("Car latest positions details",styles['Title']))
        data = [['Address',         'Date Time',         'Vitesse',         'Temperature',         'Fuel']]
        
        for waypoint in items:
            adresse = str(waypoint.adresse).encode('utf8')
            data += [[adresse,  waypoint.date.strftime("%d-%m %H:%M"),  str(waypoint.vitesse),  str(waypoint.temperature), str(waypoint.carburant)]]
        
        data += [['',    '',    '',    '',    '']]
         
        # First the top row, with all the text centered and in Times-Bold,
        # and one line above, one line below.
        ts = [('ALIGN', (1,1), (-1,-1), 'CENTER'),
             ('LINEABOVE', (0,0), (-1,0), 1, colors.purple),
             ('LINEBELOW', (0,0), (-1,0), 1, colors.purple),
             ('FONT', (0,0), (-1,0), 'Times-Bold'),
         
        # The bottom row has one line above, and three lines below of
        # various colors and spacing.
             ('LINEABOVE', (0,-1), (-1,-1), 1, colors.purple),
             ('LINEBELOW', (0,-1), (-1,-1), 0.5, colors.purple,
              1, None, None, 4,1),
             ('LINEBELOW', (0,-1), (-1,-1), 1, colors.red),
             ('FONT', (0,-1), (-1,-1), 'Times-Bold')]
         
        # Create the table with the necessary style, and add it to the
        # elements list.
        table = Table(data, style=ts)
        elements.append(table)
         
        # Write the document to disk
        doc.build(elements)
                
        
        return response



def json_get_ride(request):
    if request.user.is_authenticated():
                to_json = []
                s = request.GET.get('data', '').encode('UTF-8')
                
#                 cursor = connection.cursor()
#                 query_get_voiture_id="select voiture,dateheure from archive where idarchive = '%s' "% (s)
#                 cursor.execute(query_get_voiture_id)
#                 item = cursor.fetchone()
               
                query_get_ride_level = 'select DISTINCT (geo_pos) as geo_position, idarchive as id, X(geo_pos) as lng, Y(geo_pos) as lat, dateheure as date,vitesse as vitesse,temperature as temperature,niveau_carburant as carburant, adresse as adresse from archive where voiture =%s order by dateheure DESC limit 50' %(s)
                items = Ride.objects.raw(query_get_ride_level)
                for pos in items:
                    ride_dict = OrderedDict()
                    ride_dict['idarchive'] = pos.id
                    ride_dict['geo_pos_X'] = pos.lng
                    ride_dict['geo_pos_Y'] = pos.lat
                    ride_dict['vitesse'] = pos.vitesse
                    ride_dict['temperature'] = pos.temperature
                    ride_dict['carburant'] = pos.carburant
                    ride_dict['date']=pos.date.strftime("%d-%m %H:%M")
                    ride_dict['adresse']=pos.adresse
                    ride_dict['id_voiture']=(s)

                    
                    to_json.append(ride_dict)                    
                
                root_name = 'rows' # or it can be queryset.model._meta.verbose_name_plural 
                data = '{"%s": %s }' % ( root_name, json.dumps(to_json, cls=DjangoJSONEncoder,indent=4)) 
                
                #js = '{root:%s,count:%s}' % (serializers.serialize('json', to_json, use_natural_keys=True), to_json.count())
                return HttpResponse(data, content_type='application/json')

            




def json_get_fuel_consom(request):
    if request.user.is_authenticated():
                to_json = []
                s = request.GET.get('data', '').encode('UTF-8')
                cursor = connection.cursor()
                query_get_voiture_id="select voiture,dateheure from archive where idarchive = '%s' "% (s)
                cursor.execute(query_get_voiture_id)
                item = cursor.fetchone()
#                 
#                 query_get_voiture_id="select dateheure from mobile where idarchive = '%s' "% (item[1])
#                 cursor.execute(query_get_voiture_id)
#                 item = cursor.fetchone()
                
                query_get_fuel_level = 'select idarchive as id, niveau_carburant as value, dateheure as date from archive where voiture =%s AND niveau_carburant >0 order by dateheure' %(item[0])
                items = Fuel.objects.raw(query_get_fuel_level)
#                 { 
#                  cols: [{label: 'Date', type: 'date'},
#                         {label: 'Sold Pencils', type: 'number'},
#                         ],     
#                  rows: [
#                    {c:[{v: new Date(2008, 1 ,1)}, {v: 30000}]},
#                    {c:[{v: new Date(2008, 1 ,2)}, {v: 14045}]},          
#                },   0.5);  
                for fuel in items:
                    fuel_dict = OrderedDict()
                    fuv1_dict = OrderedDict()
                    fuv2_dict = OrderedDict()
                    to_fu = []      
                    fuv1_dict['v']="Date("+str(fuel.date)[:4]+","+ str(fuel.date)[5:7]+","+ str(fuel.date)[8:10]+","+str(fuel.date)[11:13]+","+str(fuel.date)[14:16]+","+str(fuel.date)[17:19]+")"              
#                     fuv1_dict['v']=str(fuel.date)
                    to_fu.append(fuv1_dict)                    
                    fuv2_dict['v']=fuel.value
                    to_fu.append(fuv2_dict)                    
                    fuel_dict['c'] = to_fu
                    to_json.append(fuel_dict)                    
                cols = 'cols'
                schema = '[{"label": "Date", "type": "datetime"},\n         '
                schema+='{"label": "carburant", "type": "number"}\n        ]'
                root_name = 'rows' # or it can be queryset.model._meta.verbose_name_plural 
                data = '{\n"%s": %s,\n"%s": %s }' % (cols,schema, root_name, json.dumps(to_json, cls=DjangoJSONEncoder,indent=4)) 
                
                #js = '{root:%s,count:%s}' % (serializers.serialize('json', to_json, use_natural_keys=True), to_json.count())
                return HttpResponse(data, content_type='application/json')

                 
def json_get_fuel_consom1(request):
    if request.user.is_authenticated():
                to_json = []
                s = request.GET.get('data', '').encode('UTF-8')
                cursor = connection.cursor()
                query_get_voiture_id="select id_mobile,last_id_archive from mobile where matricule = '%s' "% (s)
                cursor.execute(query_get_voiture_id)
                item = cursor.fetchone()
#                 
#                 query_get_voiture_id="select dateheure from mobile where idarchive = '%s' "% (item[1])
#                 cursor.execute(query_get_voiture_id)
#                 item = cursor.fetchone()
                
                query_get_fuel_level = 'select idarchive as id, niveau_carburant as value, dateheure as date from archive where voiture =%s AND niveau_carburant >0 order by dateheure limit 30' %(item[0])
                items = Fuel.objects.raw(query_get_fuel_level)
                for fuel in items:
                    
                    fuel_dict = OrderedDict()
                    fuv1_dict = OrderedDict()
                    fuv2_dict = OrderedDict()
                    to_fu = []                    
                    fuv1_dict['v']=str(fuel.date)
                    to_fu.append(fuv1_dict)                    
                    fuv2_dict['v']=fuel.value
                    to_fu.append(fuv2_dict)                    
                    fuel_dict['c'] = to_fu
                    
                    to_json.append(fuel_dict)                    
                cols = 'cols'
                schema = '[{"id": "dateheure", "label": "DateHeure", "type": "string"},{"id": "carburant", "label": "Carburant", "type": "number"}]'
                root_name = 'rows' # or it can be queryset.model._meta.verbose_name_plural 
                data = '{"%s": %s,\n"%s": %s }' % (cols,schema, root_name, json.dumps(to_json, cls=DjangoJSONEncoder,indent=4)) 
                
                #js = '{root:%s,count:%s}' % (serializers.serialize('json', to_json, use_natural_keys=True), to_json.count())
                return HttpResponse(data, content_type='application/json')


def json_get_car_information(request):
    if request.user.is_authenticated():
                to_json = []
                s = request.GET.get('data', '').encode('UTF-8')
                cursor = connection.cursor()
                query_get_voiture_last_id_archive="select last_id_archive from mobile where matricule = '%s' "% (s)
                cursor.execute(query_get_voiture_last_id_archive)
                item = cursor.fetchone()
                
                query_get_car_information = 'select idarchive as id, vitesse as vitesse, niveau_carburant as carburant,temperature as temperature, angle as direction from archive where idarchive =%s ' %(item[0])
                items = Charts.objects.raw(query_get_car_information)
                for chart in items:
                    chart_dict = OrderedDict()
                    chart_dict['vitesse'] = chart.vitesse
                    chart_dict['carburant'] = chart.carburant
                    chart_dict['temperature'] = chart.temperature
                    chart_dict['angle'] = chart.direction
                                        
                    to_json.append(chart_dict)                    
                
                # or it can be queryset.model._meta.verbose_name_plural 
                data =  json.dumps(to_json, cls=DjangoJSONEncoder,indent=4) 
                
                #js = '{root:%s,count:%s}' % (serializers.serialize('json', to_json, use_natural_keys=True), to_json.count())
                return HttpResponse(data, content_type='application/json')


def UserProRegistration(request):
        if request.user.is_authenticated():
                return HttpResponseRedirect('/profile/')
        if request.method == 'POST':
                form = RegistrationForm(request.POST)
                if form.is_valid():
                        user = User.objects.create_user(username=form.cleaned_data['username'], email = form.cleaned_data['email'], password = form.cleaned_data['password'])
                        user.save()
                        userPro = UserPro(user=user, telephone=form.cleaned_data['telephone'], adress=form.cleaned_data['adress'])
                        userPro.save()
                        return HttpResponseRedirect('/profile/')
                else:
                        return render_to_response('register.html', {'form': form}, context_instance=RequestContext(request))
        else:
                ''' user is not submitting the form, show them a blank registration form '''
                return HttpResponseRedirect('/login/')

def LoginRequest(request):
        if request.user.is_authenticated():
            
                return HttpResponseRedirect('/index/')
        if request.method == 'POST':
                form = LoginForm(request.POST)
                if form.is_valid():
                        societe  = form.cleaned_data['societe']
                        #username = form.cleaned_data['username']
                        username = form.cleaned_data.get('username')
                        password = form.cleaned_data['password']
                        usr = username.encode('utf8')
                        pwd = password.encode('utf8')
                        societe = societe.encode('utf8')
                        
                        
                        cursor = connection.cursor()
                        #get Id provider from domain name
                        id_provider=1
                        #    id_provider init to 1 but 
                        #    Require fetching table provide by domain name to get id_provider
                        
                        if id_provider is not None:
                            #get Id societe
                            cursor.execute("SELECT id,id_pays FROM societe WHERE LOWER(raison_sociale) = LOWER(%s) AND id_provider = %s",[societe,id_provider])
                            id_societe = cursor.fetchone()
                            if id_societe is not None:
                                #get Id connexion
                                cursor.execute("SELECT id_user FROM connexion WHERE nom_user = %s AND pwd_user = %s AND id_societe=%s ",[usr,pwd,id_societe[0]])
                                id_connexion = cursor.fetchone()
                                
                                if id_connexion is not None:                                    
                                    #get or create view
                                    query_create_view ="create VIEW v_%s_%s_%s AS select A1.idarchive id, A1.position positio, A1.dop dop, A1.alltitude alltitude, A1.etat etat, A1.dateheure  dateheure, A1.vitesse  vitesse,A1.voiture id_voiture, A2.matricule  voiture, A1.chauffeur  chauffeur, A1.cap cap, A1.disponible  disponible,A1.geo_pos geo_pos, A1.id_archive_local  id_id_archive_local, A1.inputs inputs, A1.outputs outputs, A1.angle angle, A1.temperature  temperature, A1.etat_porte etat_porte,  A1.niveau_carburant  niveau_carburant, A1.id_delegation  id_delegation,A1.id_ville id_ville, A1.id_localite  id_localite, A1.batterie  batterie,A1.alimentation  alimentation, A1.gps_valid gps_valid, A1.donnee donnee,A1.adresse_source adresse_source, A1.adresse adresse , A2.numtel numtel, A2.kilometarge kilometarge, A2.consommation consommation, A2.distance_vidange distance_vidange, A2.v_max_carburant v_max_carburant, A2.capacite_carburant capacite_carburant, A2.v_min_carburant v_min_carburant, A2.survitesse survitesse, A1.flag_geocode flag_geocode FROM archive A1 , mobile A2 where A1.voiture in(SELECT id_mobile FROM ass_conn_mobile WHERE id_user in (SELECT id_user FROM connexion WHERE nom_user = %s AND pwd_user = %s))AND A1.voiture = A2.id_mobile "
                                    cursor.execute("select * from information_schema.views where table_name='v_%s_%s_%s'",[id_provider,id_societe[0],id_connexion[0]])
                                    #check if View does'nt exist --> create view v_idprovider_idsociete_iduser
                                    if(bool(cursor.rowcount)==False) :
                                        cursor.execute(query_create_view,[id_provider,id_societe[0],id_connexion[0],usr,pwd])    
                                    
                                    username+='_'+str(id_societe[0])
                                    userPro = authenticate(username=username, password=password)
                                    if userPro is not None:
                                         login(request, userPro)
                                         #get zoom_point Societe's Pays 
                                         cursor.execute("SELECT zoom_point, zoom FROM pays WHERE id_pays = %s ",[id_societe[1]])
                                         zoom_point = cursor.fetchone()
                                
                                         request.session['zoom_point'] = zoom_point[0]
                                         request.session['zoom'] = zoom_point[1]
                                         request.session['id_connexion'] = id_connexion[0]
                                         request.session['id_societe'] = id_societe[0]
                                         request.session['id_provider']=id_provider
                                         return redirect('/index/')
                            
                                    else:
                                            form = LoginForm()
                                            context = {'form': form}
                                            return render_to_response('login_form.html', context, context_instance=RequestContext(request))
                                else :
                                        form = LoginForm()
                                        context = {'form': form}
                                        return render_to_response('login_form.html', context, context_instance=RequestContext(request))
                            else:
                                    form = LoginForm()
                                    context = {'form': form}
                                    return render_to_response('login_form.html', context, context_instance=RequestContext(request))    #return msg "username & password failed
                        else:
                                form = LoginForm()
                                context = {'form': form}
                                return render_to_response('login_form.html', context, context_instance=RequestContext(request))
                            #return msg "societe does'nt existe        
                        #return msg "provider does'nt exist in data base    
                else:
                        form = LoginForm()
                        context = {'form': form}
                        return render_to_response('login_form.html', context, context_instance=RequestContext(request))
        else:
                ''' user is not submitting the form, show the login form '''
                form = LoginForm()
                context = {'form': form}
                return render_to_response('login_form.html', context, context_instance=RequestContext(request))

def LogoutRequest(request):
        logout(request)
        return HttpResponseRedirect('/login/')
    
    
