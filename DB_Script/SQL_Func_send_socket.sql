
CREATE OR REPLACE FUNCTION sendsocket(msg character varying[], host character varying, port integer)
  RETURNS integer AS
$BODY$
  import json
  from socketIO_client import SocketIO
  try:
    
    res = '{"idarchive":%s, "position":"%s", "dop":"%s", "alltitude":"%s", "etat":"%s", "dateheure":"%s", "vitesse":"%s", "voiture":"%s", "cap":"%s", "disponible":"%s", "geo_pos_X":"%s", "geo_pos_X":"%s", "inputs":"%s", "outputs":"%s", "angle":"%s", "temperature":"%s", "etat_porte":"%s", "niveau_carburant":"%s", "batterie":"%s", "alimentation":"%s", "gps_valide":"%s","adresse":"%s","id_societe":"%s"}' % (msg[0], msg[1], msg[2], msg[3], msg[4], msg[5], msg[6], msg[7], msg[9], msg[10], msg[11], msg[12], msg[14], msg[15], msg[16], msg[17], msg[18], msg[19], msg[23], msg[24], msg[25], msg[28], msg[30])
    ws = SocketIO(host, port)
    ws.emit('pgsql', res)
    return 1
  except:
    raise
    return 0
$BODY$
  LANGUAGE plpythonu VOLATILE
  COST 100;
ALTER FUNCTION sendsocket(character varying[], character varying, integer)
  OWNER TO postgres;

