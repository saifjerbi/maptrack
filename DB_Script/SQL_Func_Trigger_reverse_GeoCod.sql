CREATE OR REPLACE FUNCTION reverse_geo_coding_using_googlemap()
  RETURNS trigger AS
$BODY$
Declare
matricule_voiture varchar;
adr varchar;
id_societe_voiture  varchar;
rowData varchar ARRAY[31];
BEGIN
adr := (select reverse_geo_coding_google(X(New.geo_pos),Y(New.geo_pos)));
New.adresse :=adr;
RAISE NOTICE '-% -- % --',New.adresse, New.idarchive;

UPDATE archive SET adresse = New.adresse, flag_geocode = 1 WHERE idarchive = New.idarchive;

id_societe_voiture := (SELECT id_societe FROM mobile WHERE id_mobile = New.voiture);
matricule_voiture := (SELECT matricule FROM mobile WHERE id_mobile = New.voiture);
rowData[0]:=New.idarchive;
rowData[1]:=New.position;
rowData[2]:=New.dop;
rowData[3]:=New.alltitude;
rowData[4]:=New.etat;
rowData[5]:=New.dateheure;
rowData[6]:=New.vitesse;
rowData[7]:=matricule_voiture;
rowData[8]:=New.chauffeur;
rowData[9]:=New.cap;
rowData[10]:=New.disponible;
rowData[11]:=X(New.geo_pos);
rowData[12]:=Y(New.geo_pos);
rowData[13]:=New.id_archive_local;
rowData[14]:=New.inputs;
rowData[15]:=New.outputs;
rowData[16]:=New.angle;
rowData[17]:=New.temperature;
rowData[18]:=New.etat_porte;
rowData[19]:=New.niveau_carburant;
rowData[20]:=New.id_delegation;
rowData[21]:=New.id_ville;
rowData[22]:=New.id_localite;
rowData[23]:=New.batterie;
rowData[24]:=New.alimentation;
rowData[25]:=New.gps_valid;
rowData[26]:=New.donnee;
rowData[27]:=New.adresse_source;
rowData[28]:=New.adresse;
rowData[29]:=New.flag_geocode;
rowData[30]:=id_societe_voiture;

PERFORM SendSocket(rowData,'localhost',83);
RETURN New;
END $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION reverse_geo_coding_using_googlemap()
  OWNER TO postgres;

