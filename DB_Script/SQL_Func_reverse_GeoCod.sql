CREATE OR REPLACE FUNCTION reverse_geo_coding_google(lat double precision, lng double precision)
  RETURNS text AS
$BODY$
    import json
    import urllib2
    from pygeocoder import Geocoder
    
    #url = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+str(lat)+","+str(lng)+"&sensor=false"
    #data = json.load(urllib2.urlopen(url))
    results = Geocoder.reverse_geocode(lng, lat)
    return results[0]
$BODY$
  LANGUAGE plpythonu VOLATILE
  COST 100;
ALTER FUNCTION reverse_geo_coding_google(double precision, double precision)
  OWNER TO postgres;

